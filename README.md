# media-core-service
  
*Микросервис для конвертации изображений с учетом задаваемых параметров.*  
  
REQUEST:  
```
{  
    "outputSettings": {  
        "format" : string,  
        "quality" : number,  
        "size" : number[],  
        "pctSize" : number[]  
    }  
    "sources" : string[]  
}
```  
```
SUPPORT_FORMATS = jpeg png bmp  
```  
  
  
HEALTHCHECK (TERMINUS) API:  
```
    /health
```  
  

SWAGGER DOC API:  
```
    /docs/api
```