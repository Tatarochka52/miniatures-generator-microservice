import { Injectable } from '@nestjs/common';
import S3 from 'aws-sdk/clients/s3';
import { config } from 'dotenv';
import { ConfigService } from '@nestjs/config';
import { BadFileFormat } from 'src/errors/bad-file-format.exception';
import { BadContentLength } from 'src/errors/bad-content-lenght.exception';

config();

const configService = new ConfigService();

enum ACL {
    'PUBLIC_READ' = 'public-read',
    'AUTHENTICATED_READ' = 'authenticated-read',
}  

@Injectable()
export class AwsService {
    private bucket: string;
    private host: string;
    private params: {
        accessKeyId: string;
        secretAccessKey: string;
        endpoint: string;
    }

    private format: {
        jpeg: string;
        png: string;
        bmp: string;
    }

    private maxFileSize: number;

    
    constructor() {
        this.bucket = configService.get<string>('AWS_BUCKET');
        this.host = configService.get<string>('AWS_HOST');
        this.params = {
            accessKeyId: configService.get<string>('AWS_ACCESS_KEY_ID'),
            secretAccessKey: configService.get<string>('AWS_SECRET_ACCESS_KEY'),
            endpoint: configService.get<string>('AWS_ENDPOINT')
        };

        this.format = {
            jpeg: configService.get<string>('SUPPORT_FORMATS').split(' ')[0],
            png: configService.get<string>('SUPPORT_FORMATS').split(' ')[1],
            bmp: configService.get<string>('SUPPORT_FORMATS').split(' ')[2]
        };

        this.maxFileSize = configService.get<number>('MAX_CONTENT_LENGTH');
    }

    public async getObject(link: string) {
        link = link.replace(this.host, '');
        let paths = link.split('/');
        paths = paths.filter((elm) => elm);
        const key = paths[paths.length - 1];
        paths.splice(paths.length - 1, 1);

        const s3 = new S3(this.params);
        
        paths.unshift(this.bucket);

        const file = await s3.getObject({
            Bucket: paths.join('/'),
            Key: key,
        }).promise();

        const contentType = file.ContentType.substring(file.ContentType.lastIndexOf('/')).replace('/', '');

        if(contentType != ( this.format.jpeg || this.format.png || this.format.bmp )) {
            throw new BadFileFormat(key);
        }

        if(file.ContentLength > this.maxFileSize) {
            throw new BadContentLength();
        }

        return file;    
    }

    public async putObject(image: any, link: string, key: string) {
        link = link.replace(this.host, '');
        let paths = link.split('/');
        paths = paths.filter((elm) => elm);
        paths.splice(paths.length - 1, 1);

        const s3 = new S3(this.params);

        paths.unshift(this.bucket);

        await s3.putObject({
            Body: image,
            Bucket: paths.join('/'),
            Key: key,
            ACL: ACL.PUBLIC_READ,
            ContentType: ''
        }).promise();
    }
}
