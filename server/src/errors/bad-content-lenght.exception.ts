import { HttpException, HttpStatus } from "@nestjs/common";

export class BadContentLength extends HttpException {
    message: string;

    constructor() {
        const response = `File size greater than 100 MB`;
        super(response, HttpStatus.BAD_REQUEST);
        this.message = response;
    }
}