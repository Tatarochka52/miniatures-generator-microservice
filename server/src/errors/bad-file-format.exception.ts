import { HttpException, HttpStatus } from "@nestjs/common";
import * as dotenv from 'dotenv';

dotenv.config();

export class BadFileFormat extends HttpException {
    message: string;

    constructor(fileName: string) {
        const response = `File ${fileName} has wrong format (can be .jpeg, .png, .bmp)`;
        super(response, HttpStatus.BAD_REQUEST);
        this.message = response;
    }
}