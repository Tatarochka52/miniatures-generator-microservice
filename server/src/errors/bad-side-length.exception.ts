import { HttpException, HttpStatus } from "@nestjs/common";
import { SideErrorStatus } from '../image/constants/image.constants';

export class BadSideLength extends HttpException {
    message: string;

    constructor(errorFormat: string) {
        let response:string; 

        if(errorFormat == SideErrorStatus.LARGE_FORMAT)
            response = `File side size greater than 10000px`;

        if(errorFormat == SideErrorStatus.WRONG_FORMAT)
            response = `File has wrong sizes`;
            
        super(response, HttpStatus.BAD_REQUEST);
        this.message = response;
    }
}