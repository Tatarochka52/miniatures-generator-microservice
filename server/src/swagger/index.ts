import { INestApplication } from '@nestjs/common';
import * as packageJson from '../../package.json';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import expressBasicAuth from 'express-basic-auth';
import { ConfigService } from '@nestjs/config';
import { config } from 'dotenv';

config();

const configService = new ConfigService();

export const SWAGGER_PATH = configService.get<string>(
  'SWAGGER_PATH',
  '/docs/api',
);

const options = new DocumentBuilder()
  .setTitle(packageJson.name)
  .setDescription(packageJson.description)
  .setVersion(packageJson.version)
  .build();

export function createSwagger(app: INestApplication): INestApplication {
  const login = configService.get<string>('SWAGGER_LOGIN');
  const password = configService.get<string>('SWAGGER_PASS');

  app.use(
    SWAGGER_PATH,
    expressBasicAuth({
      challenge: true,
      users: { [login]: password },
    }),
  );
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(SWAGGER_PATH, app, document);
  return app;
}