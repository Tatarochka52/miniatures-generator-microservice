import { Controller, Get } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import {
    HealthCheck,
    HttpHealthIndicator,
    HealthCheckService
} from '@nestjs/terminus';

@ApiTags('Healthcheck')
@Controller('health')
export class HealthController {
    constructor (
        private health: HealthCheckService,
        private http: HttpHealthIndicator
    ) {}

    @Get()
    @HealthCheck()
    check() {
        return this.health.check([
            async () => this.http.pingCheck('convert-service', 'http://localhost:8080')
        ]);
    };
}