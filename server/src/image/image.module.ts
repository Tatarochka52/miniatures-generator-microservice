import { Module } from '@nestjs/common';
import { AwsModule } from 'src/aws/aws.module';
import { ImageController } from './image.controller';
import { ImageService } from './image.service';

@Module({
    imports: [AwsModule],
    controllers: [ImageController],
    providers: [ImageService]
})
export class ImageModule {}
