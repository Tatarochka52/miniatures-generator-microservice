import { Controller, HttpStatus, UsePipes, ValidationPipe } from '@nestjs/common';
import { HttpCode, Post, Body } from '@nestjs/common/decorators';
import { ImageService } from './image.service';
import { ImageConvertationDto } from "./dto/images-convertation.dto";
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('Image')
@Controller('image')
export class ImageController {
    constructor(private readonly generatorService: ImageService) {}

    /**
     * Convertation package of images with settings set.
     * @param imagesConvertationDto ImageConvertationDto
     * @returns 
     */
    @ApiOperation({ summary: 'Convert images from aws with settings set' })
    @UsePipes(new ValidationPipe())
    @Post('convert')
    @HttpCode(HttpStatus.OK)
    packageConvertation(@Body() imagesConvertationDto: ImageConvertationDto) {
        return this.generatorService.packageConvertation(imagesConvertationDto);
    }
}
