import { Injectable } from "@nestjs/common";
import { AwsService } from "src/aws/aws.service";
import sharp from 'sharp';
import * as dotenv from 'dotenv';
import { ConvertationStrategy } from "./strategies/convertation.strategy";
import { ImageConvertationDto } from "./dto/images-convertation.dto";
import { BadSideLength } from "src/errors/bad-side-length.exception";
import { SideErrorStatus } from './constants/image.constants';

dotenv.config();

@Injectable()
export class ImageService {
    constructor(private awsService: AwsService) {};
    
    /**
     * Service to convertation package of images with input settings set.  
     *   
     * @param inputParams ImageConvertationDto
     */
    packageConvertation(inputParams: ImageConvertationDto) {

        inputParams.sources.map(async link => {
            const imageFromS3 = await this.awsService.getObject(link);
            
            const sharpImage = await sharp(imageFromS3.Body);

            sharpImage.metadata().then(meta => {
                if((meta.width || meta.height) > process.env.MAX_PX_LENGTH) {
                    throw new BadSideLength(SideErrorStatus.LARGE_FORMAT);
                }

                if((meta.width || meta.height) <= 0) {
                    throw new BadSideLength(SideErrorStatus.WRONG_FORMAT);
                }
            });

            ConvertationStrategy.packageConvertation(sharpImage, link, inputParams.output_settings, this.awsService);
        });
    }
}
