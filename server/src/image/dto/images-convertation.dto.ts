import { IsArray, IsNotEmpty, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class ImageConvertationDto {
    @ApiProperty({
        example: {
            format: 'png',
            quality: 80,
            size: [512, 128],
            pctSize: [50]
        }
    })
    output_settings: any;

    @ApiProperty({
        example: ['http://bucket/images/image1.png', 'http://bucket/images/image2.jpeg']
    })
    @IsArray()
    @IsNotEmpty()
    @IsString({each: true})
    sources: string[];
}