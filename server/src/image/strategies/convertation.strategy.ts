import { AwsService } from 'src/aws/aws.service';
import { SizesMod } from '../constants/image.constants';

export class ConvertationStrategy {
    static packageConvertation(image: any, link: string, settings: any, awsService: AwsService) {
        if(settings.pctSize.length > 0) {
            settings.pctSize.map(async size => {
                let key = `${link.substring(link.lastIndexOf('/'), link.lastIndexOf('.'))}`;

                if(settings.quality)
                    key += `_${settings.quality}`;

                key += `_${size}.${settings.format}`;

                key = key.replace('/', '');

                const convertImage = await this.imageConverter(image, SizesMod.PERCENT, size, settings.quality, settings.format);

                await awsService.putObject(convertImage, link, key);
            });
        }

        if(settings.size.length > 0) {
            settings.size.map(async size => {
                let key = `${link.substring(link.lastIndexOf('/'), link.lastIndexOf('.'))}`;

                if(settings.quality)
                    key += `_${settings.quality}`;

                key += `_${size}.${settings.format}`

                key = key.replace('/', '');

                const convertImage = await this.imageConverter(image, SizesMod.SIZE, size, settings.quality, settings.format);

                await awsService.putObject(convertImage, link, key);
            });
        }
    }

    private static imageConverter(image: any, mod: string, size: number, outputQuality: number, format: string) {
        return image 
                .metadata()
                .then(meta => {
                    if(meta.width >= meta.height) {
                        let newSize = size;

                        if(mod == SizesMod.PERCENT)
                            newSize = Math.ceil(meta.width / 100 * size); 

                        return image 
                                .resize({ width: newSize })
                                .jpeg({ quality: outputQuality })
                                .toFormat(format)
                                .toBuffer();
                    } else {
                        let newSize = size;

                        if(mod == SizesMod.PERCENT)
                            newSize = Math.ceil(meta.height / 100 * size); 

                        return image 
                                .resize({ height: newSize })
                                .jpeg({ quality: outputQuality })
                                .toFormat(format)
                                .toBuffer();
                    }
                });
    }
}