export enum SizesMod {
    PERCENT = 'percent',
    SIZE = 'size'
}

export enum SideErrorStatus {
    LARGE_FORMAT = 'large',
    WRONG_FORMAT = 'wrong'
}